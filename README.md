# Period Timeline

A jQuery plugin for showing a horizontal timeline of events which are relative to different parallel and sequential time periods. The plugin supports dragging and mouse scrolling, fully styleable and has no dependencies.

![Period Timeline](https://bytebucket.org/zapletin/jquery.periodtimeline/raw/bf828b55b79db4796c8d1805f37893aaa3f09e58/screenshot.png)

## Browser Compatibility

| Chrome | Safari | Internet Explorer | Edge  | Firefox | Opera | Chrome Mobile | Safari Mobile |
| :----: | :----: | :---------------: | :---: | :-----: | :---: | :-----------: | :-----------: |
| ✓ 26   | ✓ 6.1  | ✓ 9              | ✓ 12  | ✓ 16    | ✓ 15  | ✓ 4.4        | ✓ 7.1         |

## Usage

### Mark up your timeline content

Timeline markup consists of a root container which contains unlimited number of period containers which contain unlimited number of events. Everything inside event containers will be transformed into event cards above the timeline.

```html
<!-- root container -->
<div id="timeline" data-start="2016-09-15">

  <!-- first period container -->
  <div data-start="2016-10-01" data-end="2016-10-15" data-name="Example period">
    <!-- event container -->
    <div data-date="2016-10-10" data-label="Every event can have a label">
      <h4>Example event 1</h4>
      <p>It can contain any HTML-markup</p>
    </div>
    <!-- event container -->
    <div data-date="2016-10-10">
      <h4>Example event 2</h4>
      <p>It can contain any HTML-markup</p>
    </div>

    ...

  </div>

  <!-- second period container -->
  <div data-start="2016-10-20">
    <!-- event container -->
    <div data-date="2016-10-25">
      <h4>Example event 3</h4>
      <p>It can contain any HTML-markup</p>
    </div>
    <!-- event container -->
    <div data-date="2016-11-05">
      <h4>Example event 4</h4>
      <p>It can contain any HTML-markup</p>
    </div>

    ...

  </div>

  ...

</div>
```

Each type of container has some controlling data-attributes.

**Root Container**

- `data-start` - set the starting date of the whole timeline. If not defined, it will be automatically set to the earliest period starting date, however it is *recommended* to manually define this attribute to speed up the initial rendering.

**Period Container**

- `data-start` *(required)* - set the starting date of the period.
- `data-end` - set the end date of the period. If not defined, it will be set to today.
- `data-name` - the name of the period. It will be displayed on the timeline, however it is not required. Events inside unnamed periods still will be displayed on the timeline.

**Event Container**

- `data-date` *(required)* - set the date of the event.
- `data-label` - set the label for the date point on the timeline.

### Include jQuery (v3.0+)

```html
<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
```

### Include the plugin and the default stylesheet

```html
<script src="path/to/jquery.periodTimeline.min.js"></script>
<link rel="stylesheet" href="path/to/periodTimeline.min.css">
```

### Initialize the plugin on your timeline container

```js
$('#timeline').timeline();
```

## Options

You can pass options on plugin initialization:

```js
$('#timeline').timeline({
    // dayWidth: 10,
    // startDate: 'latest',
    // ...
});
```

### dayWidth

- Type: `integer`
- Default: `30`

Set the width of one day on the timeline in pixels. This option helps to scale the timeline depending on the duration of your periods.

### startDate

- Type: `string`
- Default: `earliest`

Set the initial timeline date. There must be a relative event in one of the timeline periods. You can also provide `earliest` or `latest` value which will automatically set the corresponding date.

### labels.years

- Type: `boolean` or `function`
- Default: `false`

Define if you want years labels to be rendered. You can just provide a `boolean` value or a `function` that must return a `string` which will be used as the label text.

```js
$('#timeline').timeline({
    labels: {
        years: function(date) {
            return date.getFullYear();
        }
    }
});
```

### labels.months

- Type: `boolean` or `function`
- Default: `false`

Define if you want months labels to be rendered. You can just provide a `boolean` value or a `function` that must return a `string` which will be used as the label text.

```js
$('#timeline').timeline({
    labels: {
        months: function(date) {
            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            return months[date.getMonth()];
        }
    }
});
```

### labels.days

- Type: `boolean` or `function`
- Default: `false`

Define if you want days labels to be rendered. You can just provide a `boolean` value or a `function` that must return a `string` which will be used as the label text.

```js
$('#timeline').timeline({
    labels: {
        days: function(date) {
            return date.getDate();
        }
    }
});
```

### mouseWheel

- Type: `boolean`
- Default: `true`

Enable/disable the ability to scroll the timeline while scrolling a mouse wheel. Use a `boolean` value to control both the time line and event cards, or provide an `object` containing separate values, e.g. `{ timeline: true, cards: false }`.

### invertMouseWheel

- Type: `boolean`
- Default: `false`

Set to `true` if you want the timeline to scroll backwards while scrolling down with a mouse wheel. It is useful if your timeline is initiated on the latest date.

### draggable

- Type: `boolean` or `object`
- Default: `false`

Enable/disable the ability to scroll the timeline while dragging with a mouse. Use a `boolean` value to control both the time line and event cards, or provide an `object` containing separate values, e.g. `{ timeline: true, cards: false }`.

## Methods

### go

Scroll the timeline to a specified date. There must be a relative event in one of the timeline periods.

```js
$('#timeline').timeline('go', '2017-12-21');
```

## Events

### datechange

Triggered when the timeline scrolls to a date.

```js
$("#timeline").on("datechange", function(event, date) {
  // do something with the date
});
```

## Styling

The default stylesheet is written in SCSS and consists of two files:

- **\_skeleton.scss** - the core styles required for proper plugin functioning.
- **periodTimeline.scss** - everything else that is responsible only for timeline appearance.

All class names are self-explanatory, so just make a copy of periodTimeline.scss and change it according to your desires.

It is **NOT recommended** to *redefine* the default stylesheet within a page or in an additional CSS file. Compile your own stylesheet as described above.
