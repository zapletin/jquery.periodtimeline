/*!
 * Period Timeline v1.0
 * https://bitbucket.org/zapletin/jquery.periodtimeline
 * Copyright 2018 Dmitry Zapletin
 * Licensed under the MIT license
 */

(function($) {

    $.fn.timeline = function(method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('I don\'t know "' + method + '" method');
        }

    };

    // default options
    $.fn.timeline.defaults = {
        dayWidth: 30, // px
        startDate: 'earliest', // 'earliest', 'latest' or date value
        labels: {
            years: false,
            months: false,
            days: false
        },
        mouseWheel: true,
        invertMouseWheel: false,
        draggable: false
    };

    // default class names
    $.fn.timeline.classnames = {
        container: 'tl-container',
        display: 'tl-display',
        timeline: 'tl-main',
        startSpacer: 'tl-start',
        endSpacer: 'tl-end',
        baseline: 'tl-baseline',
        period: 'tl-period',
        periodCaption: 'tl-caption',
        point: 'tl-point',
        card: 'tl-card',
        dragging: 'tl-dragging',
        label: 'tl-label',
        dateLabel: 'tl-date-label',
        dateLabels: {
            years: 'tl-date-label-year',
            months: 'tl-date-label-month',
            days: 'tl-date-label-day'
        }
    };

    var methods = {

        init: function(options) {
            var settings = $.extend({}, $.fn.timeline.defaults, options);

            return this.each(function() {
                var data = $(this).data('display');
                if (data) return $.error('Timeline has been already initialized on this element');

                var timeline = $(this).addClass($.fn.timeline.classnames.timeline),
                    container = $('<div/>').addClass($.fn.timeline.classnames.container),
                    display = $('<div/>').addClass($.fn.timeline.classnames.display),
                    baseline = $('<div/>').addClass($.fn.timeline.classnames.baseline),
                    starter = $('<div/>').addClass($.fn.timeline.classnames.startSpacer),
                    ender = $('<div/>').addClass($.fn.timeline.classnames.endSpacer);

                timeline.wrap(container);
                display
                    .css('padding-left', '50%')
                    .css('padding-right', '50%')
                    .insertBefore(timeline);
                container = timeline.parent();

                var tlStart = timeline.data('start');
                if (tlStart) {
                    tlStart = new Date(tlStart);
                } else {
                    timeline.children().each(function() {
                        var start = $(this).data('start');
                        if (!start) return $.error('Start date is required for periods!');
                        start = new Date(start);
                        if (start < tlStart || !tlStart) {
                            tlStart = start;
                        }
                    });
                }

                var tlWidth = 0;

                var tlEnd = timeline.data('end');
                if (tlEnd) {
                    tlEnd = new Date(tlEnd);
                    tlWidth = Math.round((tlEnd - tlStart) / (1000 * 60 * 60 * 24)) * settings.dayWidth;
                }

                var lines = [tlStart.getTime()];
                var line = 1;
                var firstPeriodStart = null;

                timeline.children().addClass($.fn.timeline.classnames.period).each(function(period_id) {
                    var period = $(this);

                    var start = new Date(period.data('start'));
                    if (!start) return $.error('Start date is required for periods!');
                    var end = period.data('end');
                    if (!end)
                        end = new Date();
                    else
                        end = new Date(end);

                    var shortLineEnd = Math.min.apply(null, lines);
                    if (start.getTime() < shortLineEnd) {
                        line = lines.push(end.getTime());
                    } else {
                        line = lines.indexOf(shortLineEnd);
                        lines[line] = end.getTime();
                        line++;
                    }

                    var left = Math.round((start - tlStart) / (1000 * 60 * 60 * 24)) * settings.dayWidth;
                    var width = Math.round((end - start) / (1000 * 60 * 60 * 24)) * settings.dayWidth;
                    if (left + width > tlWidth)
                        tlWidth = left + width;
                    if (firstPeriodStart == null || firstPeriodStart > left)
                        firstPeriodStart = left;

                    period
                        .css('left', 'calc(' + left + 'px + 50%)')
                        .css('width', width + 'px');

                    period.children().addClass($.fn.timeline.classnames.point).each(function() {
                        var date = new Date($(this).data('date'));
                        if (date > end || date < start) $.error('Event date is out of the specified period: ' + $(this).data('date'));
                        var left = Math.round((date - start) / (1000 * 60 * 60 * 24)) * settings.dayWidth + 'px';
                        $(this).css('left', left);

                        var pointLabel = $(this).data('label');
                        if (pointLabel)
                            $('<div/>')
                                .addClass($.fn.timeline.classnames.label)
                                .css('left', left)
                                .text(pointLabel)
                                .appendTo($(this).parent());

                        var card = $('<div/>')
                            .addClass($.fn.timeline.classnames.card)
                            .attr('data-period', period_id)
                            .attr('data-date', $(this).data('date'))
                            .appendTo(display);
                        $(this).children().detach().appendTo(card);
                    });

                    var caption = period.data('name');
                    if (caption) {
                        caption = $('<div/>')
                            .addClass($.fn.timeline.classnames.periodCaption)
                            .text(caption)
                            .appendTo(period);
                        caption.css('bottom', '-' + caption.outerHeight(true) * line + 'px')
                    }
                });

                // setting timeline height
                timeline.css('height',
                    timeline.children('.' + $.fn.timeline.classnames.period).outerHeight(true)
                    + (timeline.find('.' + $.fn.timeline.classnames.periodCaption).outerHeight(true) || 1) * (lines.length+1)
                    + 'px'
                );

                // positioning start/end spacers and the main background line
                starter
                    .css('width', 'calc(' + firstPeriodStart + 'px + 50%)')
                    .css('left', '0')
                    .prependTo(timeline);

                baseline
                    .css('width', tlWidth + 'px')
                    .css('left', 'calc(' + firstPeriodStart + 'px + 50%)')
                    .prependTo(timeline);

                ender
                    .css('width', '50%')
                    .css('left', 'calc(' + tlWidth + 'px + 50%)')
                    .prependTo(timeline);

                // sorting event cards by date
                display.children().sort(function(a, b) {
                    var an = a.getAttribute('data-date'),
                        bn = b.getAttribute('data-date');
                    if (an > bn) {
                        return 1;
                    }
                    if (an < bn) {
                        return -1;
                    }
                    return 0;
                }).detach().appendTo(display);

                // drawing date labels
                if (!tlEnd) tlEnd = Math.max.apply(null, lines);
                var labels = {
                    years: {
                        print: function(d) { return d.getFullYear(); },
                        inc: function(d) { return d.setFullYear(d.getFullYear() + 1); }
                    },
                    months: {
                        print: function(d) {
                            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                            return months[d.getMonth()];
                        },
                        inc: function(d) { return d.setMonth(d.getMonth() + 1); }
                    },
                    days: {
                        print: function(d) { return d.getDate(); },
                        inc: function(d) { return d.setDate(d.getDate() + 1); }
                    }
                };
                for (ln in settings.labels) {
                    if (!settings.labels[ln]) continue;
                    var date = new Date(tlStart);
                    date.setDate(1);
                    while (date < tlEnd) {
                        var text = typeof settings.labels[ln] === 'function' ? settings.labels[ln](date) : labels[ln].print(date);
                        var label = $('<div/>')
                                        .addClass($.fn.timeline.classnames.dateLabel)
                                        .addClass($.fn.timeline.classnames.dateLabels[ln])
                                        .text(text);
                        var left = Math.round((date - tlStart) / (1000 * 60 * 60 * 24)) * settings.dayWidth;
                        label.css('left', 'calc(' + left + 'px + 50%)');
                        if (date >= tlStart)
                            label.appendTo(timeline);
                        labels[ln].inc(date);
                    }
                }

                // setting the initial data-attributes
                timeline.data({
                    container: container,
                    display: display,
                    scrollingTimeline: null,
                    scrollingDisplay: null,
                    scrolling: null,
                    dCursor: container.width() / 2,
                    invertMouseWheel: settings.invertMouseWheel
                });

                // binding all necessary events depending on specified options
                $(document).ready(function() {
                    $(window).resize(timeline, onWindowResize);
                });

                timeline.on('scroll', timeline, onTimelineScroll);
                display.on('scroll', timeline, onDisplayScroll);

                timeline.find('.' + $.fn.timeline.classnames.point).click(timeline, onPointClick);
                timeline.find('.' + $.fn.timeline.classnames.periodCaption).click(timeline, onPeriodClick);
                display.children('[data-date]').click(timeline, onCardClick);

                if (settings.mouseWheel === true || settings.mouseWheel.timeline === true)
                    timeline.on('wheel', timeline, onWheel);
                if (settings.mouseWheel === true || settings.mouseWheel.cards === true)
                    display.on('wheel', timeline, onWheel);

                if (settings.draggable === true || settings.draggable.timeline === true)
                    timeline.on('mousedown', timeline, onDragStart);
                if (settings.draggable === true || settings.draggable.cards === true)
                    display.on('mousedown', timeline, onDragStart);

                // setting the timeline to the initial date
                timeline.timeline('go', settings.startDate);
            });
        },

        go: function(date) {
            return this.each(function() {
                var display = $(this).data('display');
                if (!display) return $.error('Timeline has NOT been initialized on this element');
                if (date == 'earliest') {
                    date = display.children().first().data('date');
                } else if (date == 'latest') {
                    date = display.children().last().data('date');
                }
                goTo($(this), null, null, date);
            });
        }

    }

    function onWindowResize(event) {
        var timeline = event.data;
        timeline.data('dCursor', timeline.data('container').width() / 2);
    }

    function goTo(timeline, point, card, date, forceScroll) {
        var display = timeline.data('display'),
            cursor = timeline.data('dCursor');

        var scrollTimeline = true,
            scrollDisplay = true;

        if (!date)
            if (point) {
                var date = point.data('date');
                scrollTimeline = !!forceScroll;
            } else if (card) {
                var date = card.data('date');
                scrollDisplay = !!forceScroll;
            }

        if (!point)
            var point = timeline.find('.' + $.fn.timeline.classnames.point + '[data-date=' + date + ']');

        if (!card)
            var card = display.children('[data-date=' + date + ']').eq(0);

        timeline
            .children().removeClass('active')
            .children().removeClass('active');
        display.children().removeClass('active');

        point
            .addClass('active')
            .parent().addClass('active');
        card.addClass('active');

        timeline.data('scrolling', true);

        if (scrollTimeline) {
            timeline.stop().animate({
                scrollLeft: point.parent()[0].offsetLeft + point[0].offsetLeft + point.outerWidth() / 2 - cursor
            }, {
                queue: false,
                duration: 150,
                complete: function() {
                    setTimeout(function(){
                        timeline.data('scrolling', null);
                    }, 50);
                }
            });
        }

        if (scrollDisplay) {
            display.stop().animate({
                scrollLeft: card[0].offsetLeft + card.outerWidth() / 2 - cursor
            }, {
                queue: false,
                duration: 150,
                complete: function() {
                    setTimeout(function(){
                        timeline.data('scrolling', null);
                    }, 50);
                }
            });
        }

        timeline.trigger('datechange', [ date ]);
    }

    function goToClosestPoint(timeline) {
        var cursor = timeline[0].scrollLeft + timeline.data('dCursor');
        var min_distance = 999999999;

        var active_point = null;
        timeline.find('.' + $.fn.timeline.classnames.point).each(function() {
            var point = $(this);
            if (Math.abs(cursor - (point[0].offsetLeft + point.parent()[0].offsetLeft)) < min_distance) {
                active_point = point;
                min_distance = Math.abs(cursor - (point[0].offsetLeft + point.parent()[0].offsetLeft));
            }
        });

        goTo(timeline, active_point);
        timeline.data('scrollingTimeline', null);
    }

    function goToClosestCard(timeline) {
        var data = timeline.data();
        var cursor = data.display[0].scrollLeft + data.dCursor;
        var min_distance = 999999;

        var active_card = null;
        data.display.children().each(function() {
            var distance = Math.abs(cursor - ($(this)[0].offsetLeft + $(this).width() / 2));
            if (distance < min_distance) {
                active_card = $(this);
                min_distance = distance;
            }
        });

        goTo(timeline, null, active_card);
        timeline.data('scrollingDisplay', null);
    }

    function onTimelineScroll(event) {
        var timeline = event.data;
        if (timeline.data('scrolling') || $(event.currentTarget).data('dragging')) return;

        if (timeline.data('scrollingTimeline')) {
            clearTimeout(timeline.data('scrollingTimeline'));
        }

        timeline.data('scrollingTimeline', setTimeout(goToClosestPoint, 50, timeline));
    }

    function onDisplayScroll(event) {
        var timeline = event.data;
        if (timeline.data('scrolling') || $(event.currentTarget).data('dragging')) return;

        if (timeline.data('scrollingDisplay')) {
            clearTimeout(timeline.data('scrollingDisplay'));
        }

        timeline.data('scrollingDisplay', setTimeout(goToClosestCard, 50, timeline));
    }

    function onPointClick(event) {
        var timeline = event.data;
        if (timeline.data('noclick')) {
            timeline.data('noclick', false);
            event.preventDefault();
            event.stopPropagation();
            return false;
        }
        goTo(timeline, $(event.currentTarget), null, null, true);
    }

    function onPeriodClick(event) {
        var timeline = event.data;
        if (timeline.data('noclick')) {
            timeline.data('noclick', false);
            event.preventDefault();
            event.stopPropagation();
            return false;
        }
        goTo(timeline, $(event.currentTarget).parent().children('.' + $.fn.timeline.classnames.point).eq(0), null, null, true);
    }

    function onCardClick(event) {
        var timeline = event.data;
        if (timeline.data('display').data('noclick')) {
            timeline.data('display').data('noclick', false);
            event.preventDefault();
            event.stopPropagation();
            return false;
        }
        goTo(timeline, null, $(event.currentTarget), null, true);
    }

    function onWheel(event) {
        if (event.originalEvent.deltaY == 0)
            return;

        var data = event.data.data(),
            invert = data.invertMouseWheel,
            scroll = $(event.currentTarget).scrollLeft(),
            delta = event.originalEvent.deltaY;

        $(event.currentTarget).scrollLeft(scroll + delta * (invert ? -1 : 1));

        if (scroll != $(event.currentTarget).scrollLeft())
        {
            event.stopPropagation();
            event.preventDefault();
        }
    }

    function onDragStart(event) {
        $(event.currentTarget)
            .data({
                'dragging': true,
                'dragStartScroll': $(event.currentTarget).scrollLeft(),
                'dragStartX': event.originalEvent.pageX
            })
            .addClass($.fn.timeline.classnames.dragging);
        $(document)
            .on('mousemove', event.currentTarget, onDrag)
            .on('mouseup', event.currentTarget, onDragEnd);
    }

    function onDrag(event) {
        if (!$(event.data).data('dragging')) return;
        var scroll  = $(event.data).data('dragStartScroll'),
            x       = $(event.data).data('dragStartX');
        $(event.data).scrollLeft(scroll + (x - event.originalEvent.pageX));
    }

    function onDragEnd(event) {
        $(event.data)
            .data('dragging', false)
            .removeClass($.fn.timeline.classnames.dragging);
        $(document).off({
            'mousemove': onDrag,
            'mouseup': onDragEnd
        });
        if (Math.abs($(event.data).scrollLeft() - $(event.data).data('dragStartScroll')) > 10) {
            $(event.data)
                .data('noclick', true)
                .scroll();
        }
    }

})(jQuery);
